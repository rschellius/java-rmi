/*
 * Copyright (c) 2004, Oracle and/or its affiliates. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * -Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 *
 * -Redistribution in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the
 *  distribution.
 *
 * Neither the name of Oracle nor the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any
 * kind. ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND
 * WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY
 * EXCLUDED. SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL
 * NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF
 * USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL SUN OR ITS LICENSORS BE LIABLE FOR
 * ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT,
 * SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER
 * CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF
 * THE USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF SUN HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 *
 * You acknowledge that Software is not designed, licensed or
 * intended for use in the design, construction, operation or
 * maintenance of any nuclear facility.
 */
package example.hello;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import example.interfaces.hello.HelloIF;

public class Server {

	// Get a logger instance for the current class
	static Logger logger = Logger.getLogger(Server.class);

	public Server() throws RemoteException {
		logger.debug("Constructor");
		logger.debug("Done");
	}

	public static void main(String args[]) {

		String hostname = null;
		
		if(args.length != 1) {
			System.out.println("Usage: Server [hostname]");
			System.exit(1);
		} else {
			hostname = args[0];
		}
		
		// Configure logging
		try {
			PropertyConfigurator.configure(new URL("http://" + hostname + "/classes/helloworld/helloserver.logconf"));
		} catch (MalformedURLException e1) {
			System.out.println("Error - log4j not initialized.");
		}
	     
		logger.debug("Starting application ---------------------------------");
		
		System.setProperty("java.rmi.server.hostname", hostname);
		System.setProperty("java.rmi.server.codebase", "http://" + hostname + "/classes/helloworld/");
		System.setProperty("java.security.policy", "http://" + hostname + "/classes/helloworld/helloserver.policy");

		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
            logger.debug("SecurityManager installed");
		}

		try {
			logger.debug("Creating stub");
			HelloImpl obj = new HelloImpl();
			HelloIF stub = (HelloIF) UnicastRemoteObject.exportObject(obj, 0);

			logger.debug("Locating registry on host '" + hostname + "'");
			Registry registry = LocateRegistry.getRegistry(hostname);
			logger.debug("Trying to register stub using name '" + HelloIF.servicename + "'");
			registry.rebind(HelloIF.servicename, stub);
			logger.debug("Stub registered");

			logger.info("Server ready");
		}
		catch (java.rmi.ConnectException e) {
			logger.error("Could not connect: " + e.getMessage());			
		}
		catch (java.security.AccessControlException e) {
			logger.error("Could not access registry: " + e.getMessage());
			logger.error("(Are rmiregistry and the webserver running?)");
			logger.error("Exiting...");
			System.exit(0);
		}
		catch (Exception e) {
			logger.error("Server exception: " + e.toString());
			// e.printStackTrace();
		}
	}
}